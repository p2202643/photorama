// jshint browser:true, eqeqeq:true, undef:true, devel:true, esversion: 6
const aside_photo = document.getElementsByTagName('aside')[0];
const main = document.getElementById("poster");
const pexels_api_key = "a9Yg4ny3HLzvNyrF54TKsjqLS01JOUqH2KW9vFKKDEVMZGMkPv2Avywe";
document.getElementById("btn-recherche").addEventListener('click', search);

const options = {
    headers: {
        'Authorization': pexels_api_key
    }
};

let selectedZone = null;

function search() {
    const valueColor = document.getElementById('colorSelect').value;
    const query = document.getElementById('searchInput').value;
    const images = aside_photo.getElementsByTagName('img');
    Array.from(images).forEach(image => {
        image.remove();
    });
    var url;
    if (valueColor === "") {
        url = `https://api.pexels.com/v1/search?query=${query}&per_page=5&size=small`;
    } else {
        url = `https://api.pexels.com/v1/search?query=${query}&per_page=5&size=small&color=${valueColor}`;
    }
    console.log(url);
    fetch(url, options)
        .then(response => response.json())
        .then(data => {
            data.photos.forEach(photo => {
                const img = new Image();
                img.src = photo.src.original;
                img.width = 50;
                img.height = 50;

                img.addEventListener("click", function () {
                    console.log("Image cliquée : " + img.src);

                    if (selectedZone) {
                        selectedZone.classList.add("zone-avec-image");
                        selectedZone.innerHTML = '';
                        const imgClone = img.cloneNode();
                        imgClone.classList.add("img-thumbnail");
                        selectedZone.appendChild(imgClone);
                        selectedZone.classList.remove("selected");
                        selectedZone = null;
                    }
                });

                aside_photo.appendChild(img);
            });
        })
        .catch(error => {
            console.error('Error:', error);
        });

    initImageSelection();
}

function initImageSelection() {
    const images = aside_photo.getElementsByTagName('img');

    Array.from(images).forEach(img => {
        img.addEventListener('click', function () {
            console.log("Image cliquée : " + img.src);

            if (selectedZone) {
                selectedZone.classList.add("zone-avec-image");
                selectedZone.innerHTML = '';
                const imgClone = img.cloneNode();
                imgClone.classList.add("img-thumbnail");
                selectedZone.appendChild(imgClone);
                selectedZone.classList.remove("selected");
                selectedZone = null;
            }
        });
    });
}

function selectZone(zone) {
    if (selectedZone === zone) {
        selectedZone.classList.remove("selected");
        selectedZone = null;
    } else {
        if (selectedZone) {
            selectedZone.classList.remove("selected");
        }

        selectedZone = zone;
        selectedZone.classList.toggle("selected");
    }
}

for (let i = 0; i < 4; i++) {
    const zone = document.createElement("div");
    zone.classList.add("zone", "depot");

    const zoneText = document.createElement("p");
    zoneText.textContent = "Zone " + (i + 1);

    zone.addEventListener("click", function () {
        console.log("Zone cliquée : " + zoneText.textContent);
        selectZone(zone);
    });

    zone.appendChild(zoneText);
    main.appendChild(zone);
}
